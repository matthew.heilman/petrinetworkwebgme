# Petri Net Design Studio (PeNDeS)
## About The Project
PenDeS is a webgme design studio to allow you to create and simulate [Petri Nets](https://en.wikipedia.org/wiki/Petri_net).

####Petri Nets
A Petri Net is a bipartate graph with two elements (places and transitions) that are connected by arcs.  A place may contain any number
tokens/markings.  Transitions move tokens from one place to the next and are only enabled if all "In Places" to the transition contain a token.

As a mathematical tool, Petri Nets can be used to setup algebraic equations, state machines, and other mathematical governing models. (1) 
Petri Nets lend themselves rather handily to the modeling of logical systems, including those occuring in computer science or communication systems (1).  
Some examples of models that can be created are state machines (i.e. vending machines, traffic lights), parallelism, data flow, communication protocols and
synchronization controls. (1).  Examples of models can be found at: http://people.cs.pitt.edu/~chang/231/y16/231sem/semObrien.pdf.

#### Out of Scope
PeNDeS does not cover some of the variants of Petri Dishes
- Extended Petri Nets / Inhibitor Arcs
- Timed Nets / Stochastic Nets
- High Level Nets
- Colored Nets
- Finite Capacity Nets



## Installation
PeNDeS utilizes Docker to simplify the setup.
### Install applications to support PeNDeS
1. [Docker Desktop](https://docs.docker.com/desktop)
1. [NodeJS](https://nodejs.org/en/) (LTS recommended)
1. [nvm](https://github.com/nvm-sh/nvm/blob/master/README.md) (Optional -Good if you have multiple node versions installed)
1. webgme-cli (```npm install -g webgme-cli```)

### Download/clone repo and build docker containers
1. ```git clone https://gitlab.com/matthew.heilman/petrinetworkwebgme```
1. ```docker-compose up -d```
1. ```docker-compose build webgme```
1. Navigate to [http://localhost:8888 ](http://localhost:8888) to start using PeNDeS!

Everytime you make a major change, you'll need to run #4 again.

### Built With

* [WebGME](https://webgme.org/)

## Modeling
#### Included Models
Two sample Petri Net models have been included.  
1. The first one shows a Petri Net that meets four different Petri Net classifications (Free-choice, State Machine, Marked Graph, and Workflow Net).
1. The second Petri Net is an example of common model, "a traffic light".
#### Making Your Own Models
1. Launch the PeNDeS webgmi webapp. 
1. Select the PetriNet project.
1. Select Composition
1. If the Up Arrow (Click to Parent) is enabled, continue to click it until it is disabled.  You will be now at the parent.
1. Drag and drop a PetriNetwork component onto the Root page/space.
1. Rename your new PetriNetwork component.
1. Select your new PetriNetwork component and click the down arrow.
1. Drag and drop places and transitions.  
1. Connect places and transitions with lines.
1. Add tokens/markings to each place (select the place and enter a number in the Marking attribute)
1. When you model is ready, select simPN.  This will bring up the simulator
1. From the simulator, you can click on any enabled transition.  Enabled transitions are green.  Disabled transitions are red.
1. When the model has reached a deadlock (there are no more tokens allows to move) all of the transitions will be red.
1. Click the Reverse icon to reset the model (this can happen any time).
1. To understand the classification of your model, lick the ? icon.  A notification will appear.

## Acknowledgements
* [JointJS](https://www.jointjs.com/opensource)

## References
1. http://people.cs.pitt.edu/~chang/231/y16/231sem/semObrien.pdf



