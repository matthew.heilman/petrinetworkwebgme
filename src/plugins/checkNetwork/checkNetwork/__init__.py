"""
This is where the implementation of the plugin code goes.
The checkNetwork-class is imported from both run_plugin.py and run_debug.py
"""
import sys
import logging
from webgme_bindings import PluginBase

# Setup a logger
logger = logging.getLogger('checkNetwork')
logger.setLevel(logging.INFO)
handler = logging.StreamHandler(sys.stdout)  # By default it logs to stderr..
handler.setLevel(logging.INFO)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
handler.setFormatter(formatter)
logger.addHandler(handler)


class checkNetwork(PluginBase):
    def main(self):
        core = self.core
        root_node = self.root_node
        active_node = self.active_node
        META = self.META

        places = {}
        transitions = {}
        path2node = {}

        # we build the most simple graph representation possible
        nodes = core.load_children(active_node)
        for node in nodes:
          path2node[core.get_path(node)] = node
          if core.is_type_of(node, META['Place']):
            places[core.get_path(node)] = {'inTrans': [], 'outTrans': []}

          if core.is_type_of(node, META['Transition']):
            transitions[core.get_path(node)] = {'inPlace': [], 'outPlace': []}

        for node in nodes:
          if core.is_type_of(node, META['PlaceToTransition']):

            places[(core.get_path(path2node[core.get_pointer_path(node, 'src')]))]['outTrans'].append(core.get_path(path2node[core.get_pointer_path(node, 'dst')]))
            transitions[(core.get_path(path2node[core.get_pointer_path(node, 'dst')]))]['inPlace'].append(core.get_path(path2node[core.get_pointer_path(node, 'src')]))
          if core.is_type_of(node, META['TransitionToPlace']):

            places[(core.get_path(path2node[core.get_pointer_path(node, 'dst')]))]['inTrans'].append(core.get_path(path2node[core.get_pointer_path(node, 'src')]))
            transitions[(core.get_path(path2node[core.get_pointer_path(node, 'src')]))]['outPlace'].append(core.get_path(path2node[core.get_pointer_path(node, 'dst')]))



        #Check Classifications

        #Check if Free Choice Petri Net
        isFreeChoice = True
        inPlaceCount = {}

        for transition in transitions:
          theList = ''.join(sorted(transitions[transition]['inPlace']))

          currentCount = int(inPlaceCount.get(theList) or 0)

          currentCount += 1

          inPlaceCount[theList] = currentCount

          if currentCount > 1:
            isFreeChoice = False

        #logger.error('Is Free Choice: ' + str(isFreeChoice))

        #Check if State Machine
        isStateMachine = True

        for transition in transitions:

          if len(transitions[transition]['inPlace']) != 1 or len(transitions[transition]['outPlace']) != 1:
            isStateMachine = False
        #logger.error('Is State: ' + str(isStateMachine))

        #Check if Marked Graph
        isMarkedGraph = True

        for place in places:

          if len(transitions[transition]['inPlace']) != 1 or len(transitions[transition]['outPlace']) != 1:
            isMarkedGraph = False
        #logger.error('Is Marked: ' + str(isMarkedGraph))

        #Check if Workflow Net
        isWorkflowNet = False
        numPlacesNoInput = 0
        numPlacesNoSink = 0

        for place in places:

          if len(places[place]['inTrans']) == 0:
            numPlacesNoInput += 1
          if len(places[place]['outTrans']) == 0:
            numPlacesNoSink += 1

        if numPlacesNoInput == 1 and numPlacesNoSink == 1:
          isWorkflowNet = True
        #logger.error('Is Workflow: ' + str(isWorkflowNet         ))

        self.send_notification('Free Choice: ' + str(isFreeChoice) + ' / State Machine: ' + str(isStateMachine) + ' / Marked Graph = ' + str(isMarkedGraph) + ' / Workflow Net = ' + str(isWorkflowNet))

