define(['jointjs', 'css!./styles/simPNWidget.css'], function (joint) {
    'use strict';

    var WIDGET_CLASS = 'sim-p-n';

    function SimPNWidget(logger, container) {
        this._logger = logger.fork('Widget');

        this._el = container;

        this.nodes = {};
        this._initialize();

        this._logger.debug('ctor finished');
    }

    SimPNWidget.prototype._initialize = function () {

        var width = this._el.width(),
            height = this._el.height(),
            self = this;

        // set widget class
        this._el.addClass(WIDGET_CLASS);

        this._jointPN = new joint.dia.Graph;
        this._jointPaper = new joint.dia.Paper({
            el: this._el,
            width : width,
            height: height,
            model: this._jointPN,
            interactive: false
        });

        // add event calls to elements


        this._jointPaper.on('element:pointerclick', function(elementView) {
            const currentElement = elementView.model;

            if (self._webgmePN) {


                Object.keys(self._webgmePN.transitions).forEach(transitionId => {

                    if (transitionId == self._webgmePN.id2place[currentElement.id]) {

                        if (self._webgmePN.transitions[self._webgmePN.id2place[currentElement.id]].valid == true) {

                            self.fireEvent(self._webgmePN.transitions[self._webgmePN.id2place[currentElement.id]]);
                        }

                    }
                });


            }
        });

        this._webgmePN = null;
    };

    SimPNWidget.prototype.onWidgetContainerResize = function (width, height) {
        this._logger.debug('Widget is resizing...');
    };

    // State Machine manipulating functions called from the controller
    SimPNWidget.prototype.initMachine = function (machineDescriptor) {
        const self = this;


        self._webgmePN = machineDescriptor;
        self._webgmePN.current = self._webgmePN.init;
        self._jointPN.clear();
        const pn = self._webgmePN;
        pn.id2place = {}; // this dictionary will connect the on-screen id to the place id

        var initialState = 'Initial Markings\n';




        // first add the places
        Object.keys(pn.places).forEach(placeId => {
            let place = null;

            place = new joint.shapes.standard.Circle({
                position: pn.places[placeId].position,
                size: { width: 60, height: 60 },
                attrs: {
                    label : {
                        text: pn.places[placeId].name + '\n(' + pn.places[placeId].tokens + ')',
                        //event: 'element:label:pointerdown',
                        fontWeight: 'bold',
                        //cursor: 'text',
                        //style: {
                        //    userSelect: 'text'
                        //}
                    },
                    body: {
                        strokeWidth: 3,
                        cursor: 'pointer'
                    }
                }
            });

            initialState += pn.places[placeId].name + '(' + pn.places[placeId].tokens + ')\n'
            place.addTo(self._jointPN);
            pn.places[placeId].joint = place;
            pn.id2place[place.id] = placeId;
        });

        //create a text box
        var rect = new joint.shapes.standard.Rectangle();
            rect.position(10, 10);
            rect.resize(150, 150);
            rect.attr({
                body: {
                    fill: 'white'
                },
                label: {
                    text: initialState,
                    fill: 'black'
                }
            });
        rect.addTo(self._jointPN);

        Object.keys(pn.transitions).forEach(transitionId => {
            let trans = null;

            trans = new joint.shapes.standard.Rectangle({
                position: pn.transitions[transitionId].position,
                size: { width: 20, height: 60 },
                attrs: {
                    label : {
                        text: pn.transitions[transitionId].name,
                        //event: 'element:label:pointerdown',
                        fontWeight: 'bold',
                        //cursor: 'text',
                        //style: {
                        //    userSelect: 'text'
                        //}
                    },
                    body: {
                        strokeWidth: 3,
                        cursor: 'pointer'
                    }
                }
            });

            trans.addTo(self._jointPN);
            pn.transitions[transitionId].joint = trans;
            pn.id2place[trans.id] = transitionId;
        });



        Object.keys(pn.transitions).forEach(transitionId => {
            const transition = pn.transitions[transitionId];

            Object.keys(transition.outPlaces).forEach(event => {
                transition.jointNext = transition.jointNext || {};

                const link = new joint.shapes.standard.Link({
                    source: {id: transition.joint.id},
                    target: {id: pn.places[event].joint.id},
                    attrs: {
                        line: {
                            strokeWidth: 2
                        },
                        wrapper: {
                            cursor: 'default'
                        }
                    },
                    labels: [{
                        position: {
                            distance: 0.5,
                            offset: 0,
                            args: {
                                keepGradient: true,
                                ensureLegibility: true
                            }
                        },
                        attrs: {
                            text: {
                                //text: event,
                                //fontWeight: 'bold'
                            }
                        }
                    }]
                });
                link.addTo(self._jointPN);
                //transition.jointNext[event] = link;
                transition.outPlaces[event] = link;
            })

           Object.keys(transition.inPlaces).forEach(event => {
                transition.jointNext = transition.jointNext || {};
                const link = new joint.shapes.standard.Link({
                    target: {id: transition.joint.id},
                    source: {id: pn.places[event].joint.id},
                    attrs: {
                        line: {
                            strokeWidth: 2
                        },
                        wrapper: {
                            cursor: 'default'
                        }
                    },
                    labels: [{
                        position: {
                            distance: 0.5,
                            offset: 0,
                            args: {
                                keepGradient: true,
                                ensureLegibility: true
                            }
                        },
                        attrs: {
                            text: {
                                //text: event,
                                //fontWeight: 'bold'
                            }
                        }
                    }]
                });
                link.addTo(self._jointPN);
                //transition.jointNext[event] = link;
                transition.inPlaces[event] = link;

            })
        });

        self._jointPaper.updateViews();
        self._decorateMachine();

    };

    SimPNWidget.prototype.destroyMachine = function () {

    };

    SimPNWidget.prototype.fireEvent = function (event) {

        const pn = this._webgmePN;
        const self = this;

         Object.keys(event.inPlaces).forEach(inPlace => {

            const link = event.inPlaces[inPlace];
            const linkView = link.findView(self._jointPaper);
            pn.places[inPlace].tokens = parseInt(pn.places[inPlace].tokens) - 1;
            self._decorateMachine();
            linkView.sendToken(joint.V('circle', { r: 10, fill: 'black' }), {duration:500}, function() {
               self._decorateMachine();
            });
        });

         setTimeout(function(){
              Object.keys(event.outPlaces).forEach(outPlace => {
                 const link = event.outPlaces[outPlace];
                 const linkView = link.findView(self._jointPaper);
                 linkView.sendToken(joint.V('circle', { r: 10, fill: 'black' }), {duration:500}, function() {
                    pn.places[outPlace].tokens = parseInt(pn.places[outPlace].tokens) + 1
                    self._decorateMachine();
                 });
             });
         }, 1000);


    };

    SimPNWidget.prototype.resetMachine = function () {
        const pn = this._webgmePN;

        Object.keys(pn.places).forEach(placeId => {
            pn.places[placeId].joint.attr('body/stroke', '#333333');
            pn.places[placeId].tokens = pn.places[placeId].origTokens;
        });


        this._decorateMachine();
    };

    SimPNWidget.prototype._decorateMachine = function() {
        const pn = this._webgmePN;

        Object.keys(pn.places).forEach(placeId => {
            pn.places[placeId].joint.attr('body/stroke', '#333333');
            pn.places[placeId].joint.attr('label/text', pn.places[placeId].name + '\n(' + pn.places[placeId].tokens + ')');
        });

            var activeTransition = new Boolean(true);

        Object.keys(pn.transitions).forEach(transitionId => {

                activeTransition = true;

                Object.keys(pn.transitions[transitionId].inPlaces).forEach(inPlace => {


                    if (parseInt(pn.places[inPlace].tokens) < 1) {
                        activeTransition = false;
                    };
                });


                if (activeTransition) {
                    pn.transitions[transitionId].joint.attr('body/fill', 'green');
                    pn.transitions[transitionId].valid = true;
                }
                else {
                    pn.transitions[transitionId].joint.attr('body/fill', 'red');
                    pn.transitions[transitionId].valid = false;
                }
           });
    };

    SimPNWidget.prototype._setCurrentState = function(newCurrent) {
        this._webgmePN.current = newCurrent;
        this._decorateMachine();
    };


    /* * * * * * * * Visualizer event handlers * * * * * * * */

    /* * * * * * * * Visualizer life cycle callbacks * * * * * * * */
    SimPNWidget.prototype.destroy = function () {
    };

    SimPNWidget.prototype.onActivate = function () {
        this._logger.debug('SimPNWidget has been activated');
    };

    SimPNWidget.prototype.onDeactivate = function () {
        this._logger.debug('SimPNWidget has been deactivated');
    };

    return SimPNWidget;
});
